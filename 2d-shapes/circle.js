const regl = require('regl')();
const icosphere = require('icosphere');
const camera = require('regl-camera')(regl, { distance: 3 });

var mesh = icosphere(5);

// Clears the color buffer to black and the depth buffer to 1
regl.clear({
  color: [0.66, 0.9, 0.81, 1],
  depth: 1
});

var circle = regl({
  // fragment shader
  frag: `
  precision mediump float;
  uniform vec4 color;
  void main () {
    gl_FragColor = color;
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  attribute vec3 position;
  void main () {
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  uniforms: {
    color: [1, 0.82, 0.7, 1]
  },

  elements: mesh.cells
});

camera(function() {
  circle();
});
