const regl = require('regl')();

// background color
regl.clear({
  color: [0.66, 0.9, 0.81, 1],
  depth: 1
});

regl({
  // fragment shader
  frag: `
  precision mediump float;
  uniform vec4 color;
  void main () {
    gl_FragColor = color;
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec2 position;
  void main () {
    gl_Position = vec4(position, 0, 1);
  }`,

  attributes: {
    position: [[-0.5, 0.5], [-0.5, -0.5], [0.5, -0.5]]
  },

  uniforms: {
    color: [1, 0.82, 0.7, 1]
  },

  // vertex count
  count: 3
})();
