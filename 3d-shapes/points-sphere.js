const regl = require('regl')();
const icosphere = require('icosphere');
const camera = require('regl-camera')(regl, {
  distance: 4,
  zoomSpeed: 1
});

const mesh = icosphere(5);

const pointsSphere = regl({
  frag: `
  precision lowp float;
  varying vec3 color;
  void main() {
    // Circle shaped points - instead of squares
    if (length(gl_PointCoord.xy - 0.5) > 0.5) {
      discard;
    }
    gl_FragColor = vec4(color + 0.5, 1);
  }`,

  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  attribute vec3 position;
  varying vec3 color;
  void main() {
    color = position;
    gl_PointSize = 2.0;
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  elements: mesh.cells,
  primitive: 'points'
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    pointsSphere();
  });
});
