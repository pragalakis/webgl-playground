const regl = require('regl')();
const glsl = require('glslify');
const mesh = require('primitive-plane')(3, 3, 40, 40);
const camera = require('regl-camera')(regl, {
  distance: 3,
  theta: 1.6,
  phi: -1.3,
  zoomSpeed: 1
});

const plane = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
  gl_FragColor = vec4(1, 1, 1, 1);
  }`,

  // vertex shader
  vert: glsl`
  #pragma glslify: snoise3 = require(glsl-noise/simplex/3d)
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  void main () {
    vec3 newPosition = position;
    newPosition.z = snoise3(vec3(position.x * 2.0, position.y * 2.0, 0.5)) * 0.05;
    gl_Position = projection * view * vec4(newPosition, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  elements: mesh.cells,
  primitive: 'lines'
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    plane();
  });
});
