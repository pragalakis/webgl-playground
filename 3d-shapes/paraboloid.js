const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 10,
  theta: 0.4,
  zoomSpeed: 1
});

let latitudeBands = 60;
let longitudeBands = 60;
let radius = 2;

let positionData = [];

// sphere generation
// http://learningwebgl.com/blog/?p=1253
for (let latNumber = 0; latNumber <= latitudeBands; latNumber++) {
  let theta = (latNumber * Math.PI) / latitudeBands;

  for (let longNumber = 0; longNumber <= longitudeBands; longNumber++) {
    let phi = (longNumber * 2 * Math.PI) / longitudeBands;

    // paraboloid, elliptic parabolid, hyperbolic paraboloid
    let x = Math.cos(phi) * Math.sin(theta);

    // parabolic cylinder
    //let x = Math.cos(phi);

    // paraboloid, parabolic cylinder, hyperbolic paraboloid
    let y = Math.cos(theta);

    // elliptic paraboloid
    //let y = Math.cos(theta) * Math.sin(phi);

    //paraboloid
    let z = Math.sin(phi) * Math.sin(theta);

    // parabolic cylinder
    //let z = Math.pow(x, 2);

    // elliptic paraboloid
    //let z = Math.pow(x, 2) + Math.pow(y, 2);

    // hyperbolic-paraboloid
    //let z = Math.pow(x, 2) - Math.pow(y, 2);

    positionData.push(radius * x);
    positionData.push(radius * y);

    // paraboloid
    positionData.push(Math.cos(radius * z) + Math.cos(radius * z));

    // elliptic paraboloid, parabolic cylinder
    //positionData.push(radius * z);

    // hyperbolic-paraboloid
    //positionData.push(z);
  }
}

let indexData = [];
for (let latNumber = 0; latNumber < latitudeBands; latNumber++) {
  for (let longNumber = 0; longNumber < longitudeBands; longNumber++) {
    let first = latNumber * (longitudeBands + 1) + longNumber;
    let second = first + longitudeBands + 1;
    indexData.push(first);
    indexData.push(second);
    indexData.push(first + 1);

    indexData.push(second);
    indexData.push(second + 1);
    indexData.push(first + 1);
  }
}

const paraboloid = regl({
  frag: `
  precision lowp float;
  varying vec3 color;
  void main() {
    gl_FragColor = vec4(color.y, color.x, color.y, 1);
  }`,

  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  attribute vec3 position;
  varying vec3 color;
  void main() {
    color = (position + 1.0) * 0.5; 
    gl_Position = projection * view * vec4(position,1);
  }`,

  attributes: {
    position: positionData
  },

  elements: indexData
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    paraboloid();
  });
});
