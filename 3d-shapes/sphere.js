const regl = require('regl')();
const camera = require('regl-camera')(regl, { distance: 5, zoomSpeed: 0 });
const icosphere = require('icosphere');
var mesh = icosphere(5);

var sphere = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 color;
  void main () {
    gl_FragColor = vec4(color * 0.5 + 0.5, 1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  attribute vec3 position;
  varying vec3 color;
  void main () {
    color = position;
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  uniforms: {
    scale: 0.25
  },

  elements: mesh.cells
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0.66, 0.9, 0.81, 1],
    depth: 1
  });
  camera(function() {
    sphere();
  });
});
