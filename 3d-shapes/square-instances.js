const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const mesh = require('primitive-quad')();
const camera = require('regl-camera')(regl, {
  distance: 300,
  theta: 1.6,
  phi: 0.3,
  zoomSpeed: 1
});

// Number of planes (NxN)
var N = 15;

var squares = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
  gl_FragColor = vec4(1,0,1,1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  attribute vec3 offset;
  uniform mat4 projection, view;
  void main () {
    gl_Position = projection * view * vec4(position.x + offset.x, position.y, position.z + offset.z, 1);
  }`,

  attributes: {
    position: mesh.positions,

    offset: {
      buffer: regl.buffer(
        Array(N * N)
          .fill()
          .map((_, i) => {
            var x = (-1 + (2 * Math.floor(i / N)) / N) * 120;
            var z = (-1 + (2 * (i % N)) / N) * 120;
            return [x, 0.0, z];
          })
      ),
      divisor: 1
    }
  },

  elements: mesh.cells,
  instances: N * N
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    squares();
  });
});
