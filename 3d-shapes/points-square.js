const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const camera = require('regl-camera')(regl, {
  distance: 3,
  zoomSpeed: 1
});

const NUM_POINTS = 3000;

const pointBuffer = regl.buffer(
  Array(NUM_POINTS)
    .fill()
    .map(function() {
      return [
        // freq
        1,
        Math.random() - 0.5,
        Math.random() - 0.5,
        1
      ];
    })
);

const pointsSquare = regl({
  frag: `
  precision lowp float;
  void main() {
    // Circle shaped points - instead of squares
    if (length(gl_PointCoord.xy - 0.5) > 0.5) {
      discard;
    }
    gl_FragColor = vec4(1, 1, 1, 1);
  }`,

  vert: `
  precision mediump float;
  attribute vec4 freq;
  uniform mat4 projection, view;
  void main() {
    vec3 position = freq.xyz;
    gl_PointSize = 2.0;
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    freq: {
      buffer: pointBuffer
    }
  },

  count: NUM_POINTS,

  primitive: 'points'
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    pointsSquare();
  });
});
