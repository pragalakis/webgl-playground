const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 8,
  theta: 0.3,
  phi: 0.1,
  zoomSpeed: 0
});

const decagonalprismPosition = [
  [0, 0, 1.046657],
  [0.5904836, 0, 0.8641878],
  [-0.05638617, 0.5877853, 0.8641878],
  [-0.5797148, -0.112257, 0.8641878],
  [0.5340974, 0.5877853, 0.6817184],
  [0.9661914, -0.112257, 0.3864765],
  [-0.636101, 0.4755283, 0.6817184],
  [-0.9272295, -0.2938926, 0.3864765],
  [0.9098052, 0.4755283, 0.2040071],
  [0.9836156, -0.2938926, -0.2040071],
  [-0.9836156, 0.2938926, 0.2040071],
  [-0.9098052, -0.4755283, -0.2040071],
  [0.9272295, 0.2938926, -0.3864765],
  [0.636101, -0.4755283, -0.6817184],
  [-0.9661914, 0.112257, -0.3864765],
  [-0.5340974, -0.5877853, -0.6817184],
  [0.5797148, 0.112257, -0.8641878],
  [0.05638617, -0.5877853, -0.8641878],
  [-0.5904836, 0, -0.8641878],
  [0, 0, -1.046657]
];

const decagonalprismEdges = [
  [8, 12],
  [3, 7],
  [3, 6],
  [1, 5],
  [16, 12],
  [17, 19],
  [10, 6],
  [11, 14],
  [18, 15],
  [11, 15],
  [16, 19],
  [9, 5],
  [16, 13],
  [0, 2],
  [9, 13],
  [9, 12],
  [2, 6],
  [0, 1],
  [8, 4],
  [11, 7],
  [10, 14],
  [17, 15],
  [8, 5],
  [0, 3],
  [2, 4],
  [17, 13],
  [10, 7],
  [1, 4],
  [18, 14],
  [18, 19]
];

const decagonalprism = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
  gl_FragColor = vec4(0,0,0,1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  void main () {
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: decagonalprismPosition
  },

  elements: decagonalprismEdges
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [1, 1, 1, 1],
    depth: 1
  });
  camera(function() {
    decagonalprism();
  });
});
