const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const camera = require('regl-camera')(regl, {
  distance: 3,
  theta: 0.79,
  phi: 0.07,
  zoomSpeed: 1
});

// Number of points (NxN)
var N = 20;

var points = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
    // Convert point squares to circles
    float r = 0.0;
    vec2 cxy = 2.0 * gl_PointCoord - 1.0;
    r = dot(cxy, cxy);
    if (r > 1.0) {
        discard;
    }
    gl_FragColor = vec4(0, 1, 1, 1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  attribute vec3 offset;
  uniform mat4 projection, view;
  void main () {
    gl_Position = projection * view * vec4(position.x + offset.x, position.y + offset.y, position.z + offset.z, 1);
    gl_PointSize = 5.0;
  }`,

  attributes: {
    position: [1, 0, 1],

    offset: {
      buffer: regl.buffer(
        Array(N * N)
          .fill()
          .map((_, i) => {
            var x = Math.floor(i / N) / N;
            var z = (i % N) / N;
            return [x, 0, z];
          })
      ),
      divisor: 1
    }
  },

  count: 1,
  instances: N * N,
  primitive: 'points'
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    points();
  });
});
