const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const camera = require('regl-camera')(regl, {
  distance: 10,
  zoomSpeed: 0
});

const N = 10000;
const rad = 10;

// random shere points generation + random value for opacity attribute
const pointBuffer = regl.buffer(
  Array(N)
    .fill()
    .map(function() {
      let cosTheta = 2 * Math.random() - 1;
      let sinTheta = Math.sqrt(1 - cosTheta * cosTheta);
      let phi = Math.random() * Math.PI * 2;
      return [
        rad * sinTheta * Math.cos(phi),
        rad * sinTheta * Math.sin(phi),
        rad * cosTheta,
        Math.random()
      ];
    })
);

const stars = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec4 opacity;

  void main () {
    gl_FragColor = vec4(opacity);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  attribute vec4 points;
  varying vec4 opacity;

  void main () {
    opacity = vec4(0,0,0,points.a);
    vec3 position = points.xyz;
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    points: {
      buffer: pointBuffer
    }
  },

  count: N,
  primitive: 'points'
});

regl.frame(function() {
  // Setting background color
  regl.clear({ color: [0, 0, 0, 1], depth: 1 });
  camera(function() {
    stars();
  });
});
