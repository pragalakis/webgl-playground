const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const mesh = require('primitive-plane')(200, 200, 40, 40);
const mat4 = require('gl-mat4');
const camera = require('regl-camera')(regl, {
  distance: 140,
  theta: 0.78,
  phi: 0.15,
  zoomSpeed: 1
});

const cubePosition = [
  [-0.5, +0.5, +0.5],
  [+0.5, +0.5, +0.5],
  [+0.5, -0.5, +0.5],
  [-0.5, -0.5, +0.5], // positive z face.
  [+0.5, +0.5, +0.5],
  [+0.5, +0.5, -0.5],
  [+0.5, -0.5, -0.5],
  [+0.5, -0.5, +0.5], // positive x face
  [+0.5, +0.5, -0.5],
  [-0.5, +0.5, -0.5],
  [-0.5, -0.5, -0.5],
  [+0.5, -0.5, -0.5], // negative z face
  [-0.5, +0.5, -0.5],
  [-0.5, +0.5, +0.5],
  [-0.5, -0.5, +0.5],
  [-0.5, -0.5, -0.5], // negative x face.
  [-0.5, +0.5, -0.5],
  [+0.5, +0.5, -0.5],
  [+0.5, +0.5, +0.5],
  [-0.5, +0.5, +0.5], // top face
  [-0.5, -0.5, -0.5],
  [+0.5, -0.5, -0.5],
  [+0.5, -0.5, +0.5],
  [-0.5, -0.5, +0.5] // bottom face
];

const cubeElements = [
  [2, 1, 0],
  [2, 0, 3], // positive z face.
  [6, 5, 4],
  [6, 4, 7], // positive x face.
  [10, 9, 8],
  [10, 8, 11], // negative z face.
  [14, 13, 12],
  [14, 12, 15], // negative x face.
  [18, 17, 16],
  [18, 16, 19], // top face.
  [20, 21, 22],
  [23, 20, 22] // bottom face
];

N = 15;

const blocks = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 color;
  void main () {
  gl_FragColor = vec4(color, 1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  attribute vec3 offset;
  uniform mat4 projection, view;
  varying vec3 color;
  void main () {
    color = vec3(0.2,position.y,0.2);
    gl_Position = projection * view * vec4(5.0 * position.x + offset.x, 15.0 * position.y + offset.y, 5.0 * position.z + offset.z, 1);
  }`,

  attributes: {
    position: cubePosition,

    offset: {
      buffer: regl.buffer(
        Array(N * N)
          .fill()
          .map((_, i) => {
            var x = (-1 + (2 * Math.floor(i / N)) / N) * 100;
            var z = (-1 + (2 * (i % N)) / N) * 100;
            return [x, 0.0, z];
          })
      ),
      divisor: 1
    }
  },

  elements: cubeElements,
  instances: N * N
});

const plane = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
  gl_FragColor = vec4(0.2, 0, 0.2, 1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view, model;
  void main () {
    gl_Position = projection * view * model * vec4(position.x - 6.0, position.y- 6.0, 7.8, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  uniforms: {
    model: mat4.rotateX([], mat4.identity([]), 6.28 * 0.25)
  },

  elements: mesh.cells
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    plane();
    blocks();
  });
});
