const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const mesh = require('primitive-quad')();
const resl = require('resl');
const camera = require('regl-camera')(regl, {
  distance: 50,
  center: [120, 120, 120],
  up: [0.5, 0, 0],
  theta: -1.53,
  zoomSpeed: 0
});

N = 200;
const quadUvs = [[0, 1], [0, 0], [1, 0], [1, 1]];

const screens = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec2 vUv;
  uniform sampler2D texture;
  void main () {
  gl_FragColor = texture2D(texture, vUv);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  attribute vec3 offset;
  attribute vec2 uv;
  varying vec2 vUv;
  uniform mat4 projection, view;

  void main () {
    vUv = uv;
    gl_Position = projection * view * vec4(position.x + offset.x, position.y + offset.y, position.z + offset.z, 1);
  }`,

  attributes: {
    position: mesh.positions,
    uv: quadUvs,
    offset: {
      buffer: regl.buffer(
        Array(N * N)
          .fill()
          .map((_, i) => {
            var x = Math.random() * 250;
            var y = Math.random() * 250;
            var z = Math.random() * 350;
            return [x, y, z];
          })
      ),
      divisor: 1
    }
  },

  uniforms: {
    texture: regl.prop('texture')
  },

  elements: mesh.cells,
  instances: N * N
});

// load assets
resl({
  manifest: {
    texture: {
      type: 'image',
      src: 'assets/blue-screen.png',
      parser: data =>
        regl.texture({
          data: data,
          mag: 'linear',
          min: 'linear'
        })
    }
  },
  onDone: ({ texture }) => {
    regl.frame(function() {
      // Setting background color
      regl.clear({
        color: [0, 0, 0, 1],
        depth: 1
      });
      camera(function() {
        // Passing texture as a prop
        screens({ texture });
      });
    });
  }
});
