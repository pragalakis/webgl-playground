const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 8,
  theta: 0.3,
  phi: 0.1,
  zoomSpeed: 0
});

const hexagonalprismPosition = [
  [0, 0, 1.118034],
  [0.8944272, 0, 0.6708204],
  [-0.2236068, 0.8660254, 0.6708204],
  [-0.7826238, -0.4330127, 0.6708204],
  [0.6708204, 0.8660254, 0.2236068],
  [1.006231, -0.4330127, -0.2236068],
  [-1.006231, 0.4330127, 0.2236068],
  [-0.6708204, -0.8660254, -0.2236068],
  [0.7826238, 0.4330127, -0.6708204],
  [0.2236068, -0.8660254, -0.6708204],
  [-0.8944272, 0, -0.6708204],
  [0, 0, -1.118034]
];

const hexagonalprismEdges = [
  [2, 4],
  [8, 4],
  [10, 11],
  [1, 5],
  [3, 6],
  [9, 11],
  [2, 6],
  [1, 4],
  [10, 6],
  [3, 7],
  [8, 11],
  [10, 7],
  [8, 5],
  [0, 2],
  [9, 5],
  [9, 7],
  [0, 3],
  [0, 1]
];

const hexagonalprism = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
  gl_FragColor = vec4(0,0,0,1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  void main () {
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: hexagonalprismPosition
  },

  elements: hexagonalprismEdges
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [1, 1, 1, 1],
    depth: 1
  });
  camera(function() {
    hexagonalprism();
  });
});
