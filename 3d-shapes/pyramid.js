const regl = require('regl')();
const resl = require('resl');
const camera = require('regl-camera')(regl, {
  distance: 8,
  theta: 0.61,
  phi: 0.7,
  zoomSpeed: 0
});

const pyramidPosition = [
  [-2.222222224753286e-8, 0, 1.732050929228419],
  [-0.8164967724512344, 1.4142138492462215, -0.5773503371794213],
  [-0.8164968132207036, -1.4142139198609873, -0.5773503660077837],
  [1.6329929791448123, 0, -0.5773502314348086]
];

const pyramidUv = [
  [0.0, 0.0],
  [1.0, 0.0],
  [1.0, 1.0],
  [0.0, 1.0],
  [0.0, 0.0],
  [1.0, 0.0],
  [1.0, 1.0],
  [0.0, 1.0]
];

//const pyramidEdges = [[0, 3], [1, 3], [0, 1], [1, 2], [0, 2], [2, 3]];
const pyramidFaces = [[0, 3, 1], [0, 1, 2], [0, 2, 3], [3, 2, 1]];

var pyramid = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec2 vUv;
  uniform sampler2D texture;
  void main () {
  gl_FragColor = texture2D(texture,vUv);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  attribute vec2 uv;
  varying vec2 vUv;
  uniform mat4 projection, view;
  void main () {
    vUv = uv;
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: pyramidPosition,
    uv: pyramidUv
  },

  uniforms: {
    texture: regl.prop('texture')
  },

  elements: pyramidFaces
});

// load assets
resl({
  manifest: {
    texture: {
      type: 'image',
      src: 'assets/lucas-benjamin-unsplash.jpg',
      parser: data =>
        regl.texture({
          data: data,
          mag: 'linear',
          min: 'linear'
        })
    }
  },
  onDone: ({ texture }) => {
    regl.frame(function() {
      // Setting background color
      regl.clear({
        color: [0, 0, 0, 1],
        depth: 1
      });
      camera(function() {
        // Passing the pucture as a prop
        pyramid({ texture });
      });
    });
  }
});
