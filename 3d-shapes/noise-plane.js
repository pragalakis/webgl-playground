const regl = require('regl')();
const glsl = require('glslify');
const mesh = require('primitive-plane')(3, 3, 40, 40);
const camera = require('regl-camera')(regl, {
  distance: 3,
  theta: 1.6,
  phi: -1.3,
  zoomSpeed: 1
});

const plane = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 color;
  void main () {
  gl_FragColor = vec4(color, 1);
  }`,

  // vertex shader
  vert: glsl`
  #pragma glslify: cnoise3 = require(glsl-noise/classic/3d)
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  varying vec3 color;
  void main () {
    vec3 newPosition = position;
    newPosition.z = cnoise3(vec3(position.x * 10.0, position.y * 10.0, 0.5)) * 0.5;
    // color based on position z
    color = vec3(0,newPosition.z,0.1);
    gl_Position = projection * view * vec4(newPosition, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  elements: mesh.cells
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    plane();
  });
});
