const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 8,
  theta: 0.3,
  phi: 0.1,
  zoomSpeed: 0
});

const octagonalantiprismPosition = [
  [0, 0, 1.073425],
  [0.7269827, 0, 0.7897703],
  [0.3081531, 0.6584417, 0.7897703],
  [-0.4657431, 0.5581999, 0.7897703],
  [-0.702991, -0.1852227, 0.7897703],
  [1.0521, -0.1852227, 0.1049676],
  [0.8775457, 0.5581999, 0.265645],
  [-0.990805, 0.3161949, 0.265645],
  [-0.9701878, -0.4471671, 0.1049676],
  [0.7849037, -0.4471671, -0.5798351],
  [0.9088923, 0.3161949, -0.4755801],
  [-0.9594584, 0.07418979, -0.4755801],
  [-0.6450701, -0.6323897, -0.5798351],
  [0.08191263, -0.6323897, -0.8634897],
  [0.3838305, 0.07418979, -0.9997054],
  [-0.3900657, -0.02605199, -0.9997054]
];

const octagonalantiprismEdges = [
  [8, 12],
  [13, 15],
  [3, 7],
  [1, 2],
  [1, 5],
  [4, 7],
  [9, 10],
  [12, 15],
  [10, 6],
  [3, 4],
  [10, 14],
  [11, 15],
  [10, 5],
  [11, 12],
  [9, 5],
  [2, 3],
  [0, 2],
  [1, 6],
  [9, 13],
  [5, 6],
  [14, 15],
  [11, 7],
  [0, 1],
  [8, 4],
  [9, 14],
  [0, 4],
  [12, 13],
  [13, 14],
  [8, 11],
  [2, 6],
  [8, 7],
  [0, 3]
];

const octagonalantiprism = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
  gl_FragColor = vec4(0,0,0,1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  void main () {
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: octagonalantiprismPosition
  },

  elements: octagonalantiprismEdges
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [1, 1, 1, 1],
    depth: 1
  });
  camera(function() {
    octagonalantiprism();
  });
});
