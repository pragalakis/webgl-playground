const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 8,
  theta: 0.3,
  phi: 0.1,
  zoomSpeed: 0
});

const squareantiprismPosition = [
  [0, 0, 1.25928],
  [1.215563, 0, 0.3289288],
  [0.2517512, 1.189207, 0.3289288],
  [-1.111284, 0.4925857, 0.3289288],
  [-0.71206, -0.9851714, 0.3289288],
  [0.5035025, -0.9851714, -0.6014224],
  [0.6077813, 0.4925857, -0.9867865],
  [-0.7552537, -0.2040357, -0.9867865]
];

const squareantiprismEdges = [
  [5, 7],
  [3, 7],
  [1, 5],
  [1, 2],
  [0, 4],
  [6, 7],
  [4, 7],
  [3, 4],
  [2, 3],
  [0, 2],
  [1, 6],
  [5, 6],
  [4, 5],
  [0, 3],
  [2, 6],
  [0, 1]
];

const squareantiprism = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
  gl_FragColor = vec4(0,0,0,1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  void main () {
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: squareantiprismPosition
  },

  elements: squareantiprismEdges
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [1, 1, 1, 1],
    depth: 1
  });
  camera(function() {
    squareantiprism();
  });
});
