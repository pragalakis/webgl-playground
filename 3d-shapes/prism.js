const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 8,
  theta: 0.3,
  phi: 0.1,
  zoomSpeed: 0
});

const prismPosition = [
  [0, 0, 1.414214],
  [1.414214, 0, 0],
  [0, 1.414214, 0],
  [-1.414214, 0, 0],
  [0, -1.414214, 0],
  [0, 0, -1.414214]
];

const prismEdges = [
  [0, 1],
  [0, 2],
  [0, 3],
  [0, 4],
  [1, 2],
  [1, 4],
  [1, 5],
  [2, 3],
  [2, 5],
  [3, 4],
  [3, 5],
  [4, 5]
];

/*
const prismFaces = [
  [0, 1, 2],
  [0, 2, 3],
  [0, 3, 4],
  [0, 4, 1],
  [1, 4, 5],
  [1, 5, 2],
  [2, 5, 3],
  [3, 5, 4]
];
*/

var prism = regl({
  // fragment shader
  frag: `
  precision mediump float;
  uniform vec4 color;
  void main () {
  gl_FragColor = color;
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  void main () {
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: prismPosition
  },

  uniforms: {
    color: [0, 0, 0, 1]
  },

  elements: prismEdges
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [1, 1, 1, 1],
    depth: 1
  });
  camera(function() {
    prism();
  });
});
