const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 8,
  theta: 0.3,
  phi: 0.1,
  zoomSpeed: 0
});

const icosahedronPosition = [
  [0, 0, 1.175571],
  [1.051462, 0, 0.5257311],
  [0.3249197, 1, 0.5257311],
  [-0.8506508, 0.618034, 0.5257311],
  [-0.8506508, -0.618034, 0.5257311],
  [0.3249197, -1, 0.5257311],
  [0.8506508, 0.618034, -0.5257311],
  [0.8506508, -0.618034, -0.5257311],
  [-0.3249197, 1, -0.5257311],
  [-1.051462, 0, -0.5257311],
  [-0.3249197, -1, -0.5257311],
  [0, 0, -1.175571]
];

const icosahedronEdges = [
  [0, 1],
  [0, 2],
  [0, 3],
  [0, 4],
  [0, 5],
  [1, 2],
  [1, 5],
  [1, 6],
  [1, 7],
  [2, 3],
  [2, 6],
  [2, 8],
  [3, 4],
  [3, 8],
  [3, 9],
  [4, 5],
  [4, 9],
  [4, 10],
  [5, 7],
  [5, 10],
  [6, 7],
  [6, 8],
  [6, 11],
  [7, 10],
  [7, 11],
  [8, 9],
  [8, 11],
  [9, 10],
  [9, 11],
  [10, 11]
];

/*

 const icosahedronUv = [
   [0.0, 0.0],
   [1.0, 0.0],
   [1.0, 1.0],
   [0.0, 1.0],
   [0.0, 0.0],
   [1.0, 0.0],
   [1.0, 1.0],
   [0.0, 1.0],
   [0.0, 0.0],
   [1.0, 0.0],
   [1.0, 1.0],
   [0.0, 1.0],
   [0.0, 0.0],
   [1.0, 0.0],
   [1.0, 1.0],
   [0.0, 1.0]
 ];

const icosahedronFaces = [
  [0, 1, 2],
  [0, 2, 3],
  [0, 3, 4],
  [0, 4, 5],
  [0, 5, 1],
  [1, 5, 7],
  [1, 7, 6],
  [1, 6, 2],
  [2, 6, 8],
  [2, 8, 3],
  [3, 8, 9],
  [3, 9, 4],
  [4, 9, 10],
  [4, 10, 5],
  [5, 10, 7],
  [6, 7, 11],
  [6, 11, 8],
  [7, 10, 11],
  [8, 11, 9],
  [9, 11, 10]
];
  */

const icosahedron = regl({
  // fragment shader
  frag: `
  precision mediump float;
  void main () {
  gl_FragColor = vec4(0,0,0,1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  void main () {
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: icosahedronPosition
  },

  elements: icosahedronEdges
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [1, 1, 1, 1],
    depth: 1
  });
  camera(function() {
    icosahedron();
  });
});
