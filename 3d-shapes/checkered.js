const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 50,
  zoomSpeed: 1
});

// check also https://github.com/regl-project/regl/blob/gh-pages/example/cube-fbo.js
const checkered = regl({
  frag: `
  precision lowp float;
  varying vec2 uv;
  void main() {
  vec2 ptile = step(0.5, fract(uv));
    gl_FragColor = vec4(abs(ptile.x - ptile.y) * vec3(1,1,1), 1);
  }`,

  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  uniform float height, tileSize;
  attribute vec2 position;
  varying vec2 uv;
  void main() {
    uv = position * tileSize;
    gl_Position = projection * view * vec4(50.0 * position.x, height, 50.0 * position.y, 1);
  }`,

  attributes: {
    position: [-1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, 1]
  },

  uniforms: {
    tileSize: 5,
    height: -5
  },

  count: 6
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    checkered();
  });
});
