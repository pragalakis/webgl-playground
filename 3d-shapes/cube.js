const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 3,
  theta: 0.8,
  phi: 0.6,
  zoomSpeed: 0
});
const resl = require('resl');

var cubePosition = [
  [-0.5, +0.5, +0.5],
  [+0.5, +0.5, +0.5],
  [+0.5, -0.5, +0.5],
  [-0.5, -0.5, +0.5], // positive z face.
  [+0.5, +0.5, +0.5],
  [+0.5, +0.5, -0.5],
  [+0.5, -0.5, -0.5],
  [+0.5, -0.5, +0.5], // positive x face
  [+0.5, +0.5, -0.5],
  [-0.5, +0.5, -0.5],
  [-0.5, -0.5, -0.5],
  [+0.5, -0.5, -0.5], // negative z face
  [-0.5, +0.5, -0.5],
  [-0.5, +0.5, +0.5],
  [-0.5, -0.5, +0.5],
  [-0.5, -0.5, -0.5], // negative x face.
  [-0.5, +0.5, -0.5],
  [+0.5, +0.5, -0.5],
  [+0.5, +0.5, +0.5],
  [-0.5, +0.5, +0.5], // top face
  [-0.5, -0.5, -0.5],
  [+0.5, -0.5, -0.5],
  [+0.5, -0.5, +0.5],
  [-0.5, -0.5, +0.5] // bottom face
];

var cubeUv = [
  [0.0, 0.0],
  [1.0, 0.0],
  [1.0, 1.0],
  [0.0, 1.0], // positive z face.
  [0.0, 0.0],
  [1.0, 0.0],
  [1.0, 1.0],
  [0.0, 1.0], // positive x face.
  [0.0, 0.0],
  [1.0, 0.0],
  [1.0, 1.0],
  [0.0, 1.0], // negative z face.
  [0.0, 0.0],
  [1.0, 0.0],
  [1.0, 1.0],
  [0.0, 1.0], // negative x face.
  [0.0, 0.0],
  [1.0, 0.0],
  [1.0, 1.0],
  [0.0, 1.0], // top face
  [0.0, 0.0],
  [1.0, 0.0],
  [1.0, 1.0],
  [0.0, 1.0] // bottom face
];

const cubeElements = [
  [2, 1, 0],
  [2, 0, 3], // positive z face.
  [6, 5, 4],
  [6, 4, 7], // positive x face.
  [10, 9, 8],
  [10, 8, 11], // negative z face.
  [14, 13, 12],
  [14, 12, 15], // negative x face.
  [18, 17, 16],
  [18, 16, 19], // top face.
  [20, 21, 22],
  [23, 20, 22] // bottom face
];

var cube = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec2 vUv;
  uniform sampler2D texture;
  void main () {
  gl_FragColor = texture2D(texture,vUv);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  attribute vec2 uv;
  varying vec2 vUv;
  uniform mat4 projection, view;
  void main () {
    vUv = uv;
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: cubePosition,
    uv: cubeUv
  },

  uniforms: {
    texture: regl.prop('texture')
  },

  elements: cubeElements
});

// load assets
resl({
  manifest: {
    texture: {
      type: 'image',
      src: 'assets/brake.png',
      parser: data =>
        regl.texture({
          data: data,
          mag: 'linear',
          min: 'linear'
        })
    }
  },
  onDone: ({ texture }) => {
    regl.frame(function() {
      // Setting background color
      regl.clear({
        color: [0, 0, 0, 1],
        depth: 1
      });
      camera(function() {
        // Passing texture as a prop
        cube({ texture });
      });
    });
  }
});
