const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 15,
  phi: -1,
  theta: 1.57,
  zoomSpeed: 1
});

let latitudeBands = 60;
let longitudeBands = 60;
let radius = 2;

let positionData = [];

for (let latNumber = 0; latNumber <= latitudeBands; latNumber++) {
  let theta = (latNumber * 2 * Math.PI) / latitudeBands;

  for (let longNumber = 0; longNumber <= longitudeBands; longNumber++) {
    let phi = (longNumber * 2 * Math.PI) / longitudeBands;

    let x = (0.95 - radius + Math.cos(phi)) * Math.cos(theta);
    let y = (0.95 - radius + Math.cos(phi)) * Math.sin(theta);
    let z = 0.5 * Math.log(Math.pow(x, 2.0) + Math.pow(y, 2.0));

    positionData.push(radius * x);
    positionData.push(radius * y);
    positionData.push(radius * z);
  }
}

let indexData = [];
for (let latNumber = 0; latNumber < latitudeBands; latNumber++) {
  for (let longNumber = 0; longNumber < longitudeBands; longNumber++) {
    let first = latNumber * (longitudeBands + 1) + longNumber;
    let second = first + longitudeBands + 1;
    indexData.push(first);
    indexData.push(second);
    indexData.push(first + 1);

    indexData.push(second);
    indexData.push(second + 1);
    indexData.push(first + 1);
  }
}

const funnel = regl({
  frag: `
  precision lowp float;
  varying vec3 color;
  void main() {
    gl_FragColor = vec4(color, 1);
  }`,

  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  attribute vec3 position;
  varying vec3 color;
  void main() {
    color = position + 0.5;
    gl_Position = projection * view * vec4(position,1);
  }`,

  attributes: {
    position: positionData
  },

  elements: indexData
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    funnel();
  });
});
