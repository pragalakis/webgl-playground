const regl = require('regl')();
const glsl = require('glslify');
const mat4 = require('gl-mat4');
const mesh = require('primitive-plane')(3, 3, 30, 30);
const icosphere = require('icosphere')(3);
const camera = require('regl-camera')(regl, {
  distance: 1.5,
  theta: 1.57,
  phi: -1.47,
  mouse: false
});

const background = regl({
  // fragment shader
  frag: `
      precision mediump float;
      varying vec2 color;

      void main () {
        gl_FragColor = vec4(color.y + 0.4,0,0.4,1);
      }
    `,
  // vertex shader
  vert: `
      precision mediump float;
      attribute vec2 position;
      varying vec2 color;

      void main () {
        color = position;
        gl_Position = vec4(position,1,1);
      }
    `,

  attributes: {
    position: [[-5, 5], [-5, -5], [5, 0]]
  },

  elements: [0, 1, 2],
  depth: {
    enable: false,
    mask: false
  }
});

const sphere = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 color;

  void main () {
    vec3 stripe = step(0.5, fract(color));
    gl_FragColor = vec4(abs(stripe.x + 0.9) * vec3(1.0,0.0,0.5), 1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  varying vec3 color;

  void main () {
    color = vec3(position.z,1,1) * 15.0;
    gl_Position = projection * view * vec4(vec3(position.x * 0.6,position.y * 0.6,position.z * 0.6 + 0.5), 1);
  }`,

  attributes: {
    position: icosphere.positions
  },

  uniforms: {
    view: mat4.lookAt([], [3.5, 0, 0.5], [0, 0, 0], [1, 0, 1]),
    projection: ({ viewportWidth, viewportHeight }) =>
      mat4.perspective(
        [],
        Math.PI / 4,
        viewportWidth / viewportHeight,
        0.01,
        1000
      )
  },

  elements: icosphere.cells
});

const plane = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 color;

  void main () {
    gl_FragColor = vec4(color, 1);
  }`,

  // vertex shader
  vert: glsl`
  #pragma glslify: snoise3 = require(glsl-noise/simplex/3d)
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  uniform float time;
  varying vec3 color;

  void main () {
    color = vec3(position.y + 0.5,0,0.5);
    vec3 newPosition = position;
    newPosition.z = snoise3(vec3(position.x * 2.0, position.y * 2.0, 0.5 * time)) * 0.05;
    gl_Position = projection * view * vec4(newPosition, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  uniforms: {
    time: regl.context('time')
  },

  elements: mesh.cells
});

const grid = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 color;

  void main () {
    gl_FragColor = vec4(color, 1);
  }`,

  // vertex shader
  vert: glsl`
  #pragma glslify: snoise3 = require(glsl-noise/simplex/3d)
  precision mediump float;
  attribute vec3 position;
  uniform mat4 projection, view;
  uniform float time;
  varying vec3 color;

  void main () {
    color = vec3(-position.y + 0.5,0,0.5);
    vec3 newPosition = position;
    newPosition.z = snoise3(vec3(position.x * 2.0, position.y * 2.0, 0.5 * time)) * 0.05;
    gl_Position = projection * view * vec4(newPosition, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  uniforms: {
    time: regl.context('time')
  },

  elements: mesh.cells,
  primitive: 'lines'
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    background();
    plane();
    grid();
    sphere();
  });
});
