const regl = require('regl')();
const mesh = require('./skeleton.json');
const quad = require('primitive-plane')(2, 2, 40, 40);
const cylinder = require('primitive-cylinder')(0.5, 0.5, 58, 10, 10);
const resl = require('resl');
const normals = require('angle-normals');
const camera = require('regl-camera')(regl, {
  center: [10, 30, 0],
  theta: 1.5,
  distance: 120,
  zoomSpeed: 1
});

console.log(mesh);

const flag = regl({
  // fragment shader
  frag: `
    precision mediump float;
    uniform sampler2D texture;
    varying vec2 vUv;

    void main () {
      gl_FragColor = texture2D(texture, vUv);
    }`,

  // vertex shader
  vert: `
    precision mediump float;
    uniform mat4 projection, view;
    uniform float time;
    attribute vec3 position;
    attribute vec2 uv;
    varying vec2 vUv;

    void main () {
      vUv = uv;
      float amp = 1.5;
      float freq = 2.0;
      float speed = 4.0;
      float distortion = sin(position.x * freq + time * speed) * amp;
      float size = 10.0;
      vec3 pos = position * size;
      vec3 npos = vec3(1.4 * pos.x + 30.0, 0.9 * pos.y + 50.0, pos.z);
      gl_Position = projection * view * vec4(npos.x,npos.y + distortion / 2.0, distortion + npos.z, 1.0);
    }`,

  attributes: {
    position: quad.positions,
    uv: quad.uvs
  },

  uniforms: {
    texture: regl.prop('texture'),
    time: regl.context('time')
  },

  elements: quad.cells
});

const stick = regl({
  // fragment shader
  frag: `
    precision mediump float;

    void main () {
      gl_FragColor = vec4(1, 1, 1, 1);
    }`,

  // vertex shader
  vert: `
    precision mediump float;
    uniform mat4 projection, view;
    attribute vec3 position;

    void main () {
      gl_Position = projection * view * vec4(position.x + 15.0, position.y + 31.0, position.z, 1.0);
    }`,

  attributes: {
    position: cylinder.positions
  },

  elements: cylinder.cells
});

const skeleton = regl({
  // fragment shader
  frag: `
    precision mediump float;
    varying vec3 vnormal;

    void main () {
      gl_FragColor = vec4(vnormal, 1.0);
    }`,

  // vertex shader
  vert: `
    precision mediump float;
    uniform mat4 projection, view;
    attribute vec3 position, normal;
    varying vec3 vnormal;

    void main () {
      vnormal = vec3(normal.z, 0.3, 0.8);
      vec3 npos = 17.0 * position;
      gl_Position = projection * view * vec4(npos.x, npos.y + 2.0, npos.z, 1.0);
    }`,

  attributes: {
    position: mesh.positions,
    normal: normals(mesh.cells, mesh.positions)
  },

  elements: mesh.cells
});

// load assets
resl({
  manifest: {
    texture: {
      type: 'image',
      src: 'black-flag.jpg',
      parser: data =>
        regl.texture({
          data: data,
          mag: 'linear',
          min: 'linear'
        })
    }
  },
  onDone: ({ texture }) => {
    regl.frame(function() {
      // Setting background color
      regl.clear({
        color: [0, 0, 0, 1],
        depth: 1
      });
      camera(function() {
        stick();
        flag({ texture });
        skeleton();
      });
    });
  }
});
