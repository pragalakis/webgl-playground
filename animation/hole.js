const regl = require('regl')();
const camera = require('regl-camera')(regl, {
  distance: 1,
  phi: 0.0,
  theta: 1.56,
  zoomSpeed: 0
});

const N = 100000;
const radius = 2;

const pointBuffer = regl.buffer(
  Array(N)
    .fill()
    .map(function() {
      let cosTheta = 2 * Math.random() - 1;
      let phi = Math.random() * Math.PI * 2;

      let x = cosTheta * Math.cos(phi);
      let y = cosTheta * Math.sin(phi);
      let z = 100 * Math.log(Math.pow(x, 2.0) + Math.pow(y, 2.0));
      return [radius * x, radius * y, radius * z];
    })
);

const hole = regl({
  // fragment shader
  frag: `
  precision lowp float;

  void main() {
    gl_FragColor = vec4(1, 1, 1, 1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  uniform float time;
  attribute vec3 position;

  void main() {
    gl_PointSize = 1.0;
    gl_Position = projection * view * vec4(position.x, position.y, mod(10.0 * time + position.z, position.z),1);
  }`,

  attributes: {
    position: {
      buffer: pointBuffer
    }
  },

  uniforms: {
    time: regl.context('time')
  },

  count: N,

  primitive: 'points'
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  camera(function() {
    hole();
  });
});
