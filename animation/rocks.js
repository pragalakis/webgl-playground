const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const glsl = require('glslify');
const mat4 = require('gl-mat4');
const mesh = require('icosphere')(0);

const N = 60;
const rad = 10;

const angle = [];
for (let i = 0; i < N * N; i++) {
  // generate random initial angle.
  angle[i] = Math.random() * (2 * Math.PI);
}

// This buffer stores the angles of all the instanced rocks
const angleBuffer = regl.buffer({
  length: angle.length * 4,
  type: 'float',
  usage: 'dynamic'
});

const rock = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 color;

  void main () {
    gl_FragColor = vec4(color,1);
  }`,

  // vertex shader
  vert: glsl`
  #pragma glslify: snoise3 = require(glsl-noise/simplex/3d)
  precision mediump float;
  attribute vec3 position;
  attribute vec3 offset;
  attribute float angle;
  uniform mat4 projection, view, model;
  varying vec3 color;

  void main () {
    color = position + sin(1.0) - 0.5;
    vec3 vpos = position + position * snoise3(vec3(0.5,0.5,0.5));
    gl_Position = projection * view * model* vec4(+cos(angle) * vpos.x + vpos.z * sin(angle) + offset.x, vpos.y + offset.y, -sin(angle) * vpos.x + vpos.z *cos(angle) + offset.z, 1);
  }`,

  attributes: {
    position: mesh.positions,
    offset: {
      buffer: regl.buffer(
        Array(N * N)
          .fill()
          .map(function() {
            let cosTheta = 2 * Math.random() - 1;
            let phi = Math.random() * Math.PI * 2;

            let x = rad * (10 + cosTheta) * 2.0 * Math.cos(phi);
            let y = rad * (10 + cosTheta) * 2.0 * Math.sin(phi);
            return [x, y, 0];
          })
      ),
      divisor: 1
    },
    angle: {
      buffer: angleBuffer,
      divisor: 1
    }
  },

  uniforms: {
    view: mat4.lookAt([], [-300, -250, 150], [0, 0, 0], [1, 1, 0]),

    projection: ({ viewportWidth, viewportHeight }) =>
      mat4.perspective(
        [],
        Math.PI / 4,
        viewportWidth / viewportHeight,
        0.01,
        1000
      ),

    model: ({ time }) => mat4.rotateZ([], mat4.identity([]), time)
  },

  instances: N * N,

  elements: mesh.cells
});

regl.frame(function() {
  // Setting background color
  regl.clear({ color: [0, 0, 0, 1], depth: 1 });

  // rotate the rocks every frame.
  for (let i = 0; i < N * N; i++) {
    angle[i] += 0.01;
  }
  angleBuffer.subdata(angle);
  rock();
});
