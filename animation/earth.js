const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const mat4 = require('gl-mat4');
const mesh = require('icosphere')(5);
const resl = require('resl');

const N = 10000;
const rad = 10;

// random shere points generation + random value for opacity attribute
const pointBuffer = regl.buffer(
  Array(N)
    .fill()
    .map(function() {
      let cosTheta = 2 * Math.random() - 1;
      let sinTheta = Math.sqrt(1 - cosTheta * cosTheta);
      let phi = Math.random() * Math.PI * 2;
      return [
        rad * sinTheta * Math.cos(phi),
        rad * sinTheta * Math.sin(phi),
        rad * cosTheta,
        Math.random()
      ];
    })
);

const stars = regl({
  frag: `
  precision lowp float;
  varying vec4 opacity;

  void main() {
    gl_FragColor = vec4(opacity);
  }`,

  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  attribute vec4 points;
  varying vec4 opacity;

  void main() {
    opacity = vec4(0,0,0,points.a);
    vec3 position = points.xyz;
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    points: {
      buffer: pointBuffer
    }
  },

  uniforms: {
    projection: ({ viewportWidth, viewportHeight }) =>
      mat4.perspective(
        [],
        Math.PI / 2,
        viewportWidth / viewportHeight,
        0.01,
        1000
      ),
    model: mat4.identity([]),
    view: mat4.lookAt([], [0, 0, 0], [0, 0, 0], [0, 1, 0])
  },

  count: N,

  primitive: 'points'
});

const earth = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 vpos;
  uniform sampler2D texture;

  void main () {
  float lon = mod(atan(vpos.x,vpos.z)*${1 / (2 * Math.PI)},1.0);
  float lat = asin(-vpos.y*0.79-0.02)*0.5+0.5;
  gl_FragColor = texture2D(texture, vec2(lon,lat));
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  attribute vec3 position;
  varying vec3 vpos;
  uniform mat4 projection, view;

  void main () {
    vpos = position;
    gl_Position = projection * view * vec4(position, 1);
  }`,

  attributes: {
    position: mesh.positions
  },

  uniforms: {
    texture: regl.prop('texture'),
    model: mat4.identity([]),
    view: ({ tick }) => {
      const t = 0.01 * tick;
      return mat4.lookAt(
        [],
        [5 * Math.cos(t), 0, 5 * Math.sin(t)],
        [0, 0, 0],
        [0, 1, 0]
      );
    },
    projection: ({ viewportWidth, viewportHeight }) =>
      mat4.perspective(
        [],
        Math.PI / 4,
        viewportWidth / viewportHeight,
        0.01,
        1000
      )
  },

  elements: mesh.cells
});

// load assets
resl({
  manifest: {
    // https://earthobservatory.nasa.gov/images/92654/just-another-day-on-aerosol-earth
    texture: {
      type: 'image',
      src: 'earth.jpg',
      parser: data =>
        regl.texture({
          data: data,
          mag: 'linear',
          min: 'linear'
        })
    }
  },
  onDone: ({ texture }) => {
    regl.frame(function() {
      // Setting background color
      regl.clear({ color: [0, 0, 0, 1], depth: 1 });
      stars();
      earth({ texture });
    });
  }
});
