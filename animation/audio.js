const regl = require('regl')();
const resl = require('resl');
const analyser = require('web-audio-analyser');
const glsl = require('glslify');
const camera = require('regl-camera')(regl, {
  distance: 10,
  phi: 0.0,
  theta: 1.56,
  zoomSpeed: 1
});

const N = 100000;
const radius = 2;

const pointBuffer = regl.buffer(
  Array(N)
    .fill()
    .map(function() {
      let cosTheta = 2 * Math.random() - 1;
      let sinTheta = Math.sqrt(1 - cosTheta * cosTheta);
      let phi = Math.random() * Math.PI * 2;

      let x = sinTheta * Math.cos(phi);
      let y = sinTheta * Math.sin(phi);
      let z = cosTheta;
      return [radius * x, radius * y, radius * z];
    })
);

const sphere = regl({
  // fragment shader
  frag: `
  precision lowp float;

  void main() {
    gl_FragColor = vec4(1, 1, 1, 1);
  }`,

  // vertex shader
  vert: glsl`
  #pragma glslify: snoise = require('glsl-noise/simplex/4d')
  precision mediump float;
  uniform mat4 projection, view;
  uniform float time;
  uniform float waveform;
  attribute vec3 position;

  void main() {
    gl_PointSize = 1.0;
    vec3 vnorm = position; 
    vec3 vpos = position + vnorm * snoise(vec4(position, waveform)) * 0.1;
    gl_Position = projection * view * vec4(vpos.x, cos(time) + vpos.y, vpos.z,1);
  }`,

  attributes: {
    position: {
      buffer: pointBuffer
    }
  },

  uniforms: {
    time: regl.context('time'),
    waveform: regl.prop('waveform')
  },

  count: N,

  primitive: 'points'
});

// load assets
resl({
  manifest: {
    audio: {
      type: 'audio',
      src: './sample.mp3',
      stream: true
    }
  },
  onDone: ({ audio }) => {
    let data = analyser(audio, { audible: true, stereo: false });
    audio.loop = true;
    audio.play();

    regl.frame(function() {
      // Setting background color
      regl.clear({
        color: [0, 0, 0, 1],
        depth: 1
      });

      waveform = data.waveform();

      let sum = 0;
      let count = 0;
      for (let i = 0; i < waveform.length; i++) {
        count++;
        sum += waveform[i];
      }

      let avg = sum / count;

      camera(function() {
        sphere({ waveform: avg });
      });
    });
  }
});
