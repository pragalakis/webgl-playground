const regl = require('regl')({ extensions: ['angle_instanced_arrays'] });
const mesh = require('./data.json');
const mat4 = require('gl-mat4');

const N = 50;

const tv = regl({
  // fragment shader
  frag: `
    precision mediump float;
    varying vec3 color;

    void main () {
      gl_FragColor = vec4(color, 1.0);
    }`,

  // vertex shader
  vert: `
    precision mediump float;
    uniform mat4 projection, view;
    uniform float time;
    attribute vec3 position, offset;
    attribute vec2 uv;
    varying vec3 color;

    void main () {
      color = vec3(uv, 0.8);
      gl_Position = projection * view * vec4(cos(time) + position.x + offset.x, position.y + offset.y, position.z + offset.z, 1.0);
    }`,

  attributes: {
    position: mesh.positions,
    uv: mesh.uv,
    offset: {
      buffer: regl.buffer(
        Array(N * N)
          .fill()
          .map(function() {
            let x = 50 * Math.random();
            let y = 50 * Math.random();
            let z = 50 * Math.random();
            return [x, y, z];
          })
      ),
      divisor: 1
    }
  },

  uniforms: {
    time: regl.context('time'),
    view: mat4.lookAt([], [45, 45, 45], [0, 0, 0], [0, 1, 0]),
    projection: ({ viewportWidth, viewportHeight }) =>
      mat4.perspective(
        [],
        Math.PI / 4,
        viewportWidth / viewportHeight,
        0.01,
        1000
      )
  },

  instances: N * N,
  elements: mesh.cells
});

regl.frame(function() {
  // Setting background color
  regl.clear({
    color: [0, 0, 0, 1],
    depth: 1
  });
  tv();
});
