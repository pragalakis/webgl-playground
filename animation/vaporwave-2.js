const regl = require('regl')();
const quad = require('primitive-quad')();
const resl = require('resl');
const mesh = require('./alex-the-great.json');
const camera = require('regl-camera')(regl, {
  distance: 130,
  phi: 0.3,
  theta: -0.6,
  zoomSpeed: 1
});

const rectangle = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec2 vUv;
  varying float dist;
  uniform sampler2D texture;

  void main () {
    gl_FragColor = texture2D(texture, vec2(vUv.x, vUv.y));
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  uniform float scale;
  attribute vec3 position;
  attribute vec2 uv;
  varying vec2 vUv;

  void main () {
    vUv = uv;
    vec3 npos = position * scale;
    gl_Position = vec4(npos.x + 0.8, 0.9*npos.y + 0.8, npos.z, 1);
  }`,

  attributes: {
    position: quad.positions,
    uv: [[0, 1], [1, 1], [1, 0], [0, 0]]
  },

  uniforms: {
    texture: regl.prop('texture'),
    scale: 0.15
  },

  elements: quad.cells
});

const background = regl({
  // fragment shader
  frag: `
      precision lowp float;

      void main () {
        gl_FragColor = vec4(1, 0, 0.4, 1);
      }
    `,
  // vertex shader
  vert: `
      precision mediump float;
      attribute vec2 position;

      void main () {
        gl_Position = vec4(position,1,1);
      }
    `,

  attributes: {
    position: [[-5, 5], [-5, -5], [5, 0]]
  },

  elements: [0, 1, 2],
  depth: {
    enable: false,
    mask: false
  }
});

const statue = regl({
  // fragment shader
  frag: `
  precision mediump float;
  varying vec3 color;

  void main() {
    gl_FragColor = vec4(color, 1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  uniform float scale, time;
  attribute vec3 position, normal;
  varying vec3 color;
  
  void main() {
    color = vec3(normal.x, 0.1, 0.5);
    float amp = 0.5;
    float freq = 150.0;
    float speed = 20.0;
    float distortion = sin(position.x*freq + time * speed)*amp;
    gl_Position = projection * view * vec4(vec3(distortion + position.x * scale, position.y * scale - 4.9, position.z * scale + 35.0), 1);
  }`,

  attributes: {
    position: mesh.positions,
    normal: mesh.normals
  },

  uniforms: {
    time: regl.context('time'),
    scale: 0.25
  },

  elements: mesh.cells
});

const plane = regl({
  // fragment shader
  frag: `
  precision lowp float;
  varying vec2 uv;

  void main() {
    vec2 ptile = step(0.5, fract(uv));
    gl_FragColor = vec4(abs(ptile.x - ptile.y) * vec3(1,0,0.4), 1);
  }`,

  // vertex shader
  vert: `
  precision mediump float;
  uniform mat4 projection, view;
  uniform float height, tileSize;
  attribute vec2 position;
  varying vec2 uv;

  void main() {
    uv = position * tileSize;
    gl_Position = projection * view * vec4(50.0 * position.x, height, 50.0 * position.y, 1);
  }`,

  attributes: {
    position: [-1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, 1]
  },

  uniforms: {
    tileSize: 5,
    height: -5
  },

  count: 6
});

// load assets
resl({
  manifest: {
    texture: {
      type: 'image',
      src: 'vapor.jpg',
      parser: data =>
        regl.texture({
          data: data,
          mag: 'linear',
          min: 'linear'
        })
    }
  },
  onDone: ({ texture }) => {
    regl.frame(function() {
      // Setting background color
      regl.clear({
        color: [0, 0, 0, 1],
        depth: 1
      });
      camera(function() {
        background();
        rectangle({ texture });
        statue();
        plane();
      });
    });
  }
});
