// NodeConf Argentina James Halliday - fun with webgl presentation
const regl = require('regl')();
const glsl = require('glslify');
const mesh = require('icosphere')(5);
const camera = require('regl-camera')(regl, { distance: 3 });

const bloab = regl({
  frag: glsl`
    precision mediump float;
    #pragma glslify: chlorophyll = require('glsl-colormap/chlorophyll')
    varying vec3 vnorm;
    void main () {
      gl_FragColor = chlorophyll(vnorm.x * vnorm.x);
    }
   `,
  vert: glsl`
    precision mediump float;
    #pragma glslify: snoise = require('glsl-noise/simplex/4d')
    uniform mat4 projection, view;
    uniform float time;
    attribute vec3 position;
    varying vec3 vpos, vnorm;
    void main () {
      vnorm = position;
      vpos = position + vnorm * snoise(vec4(position,time))* 0.2;
      gl_Position = projection * view * vec4(vpos,1); 
    }
  `,
  attributes: {
    position: mesh.positions
  },
  elements: mesh.cells,
  uniforms: {
    time: regl.context('time')
  }
});

regl.frame(function() {
  //background color
  regl.clear({ color: [0, 0, 0, 1], depth: true });
  camera(function() {
    bloab();
  });
});
