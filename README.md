# [deprecated] webgl-playground

Moved to codeberg: https://codeberg.org/evasync/webgl-playground

WebGL playground

## 2d-shapes

<table>
  <tr>
    <td width="300" valign="top">
      <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/triangle.png"><img src="./screenshots/triangle.png" alt="triangle 2d" width="290"></a>
      <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/2d-shapes/triangle.js">Triangle</a>
    </td>
    <td width="300" valign="top">
      <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/rectangle.png"><img src="./screenshots/rectangle.png" alt="rectangle 2d" width="290"></a>
      <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/2d-shapes/rectangle.js">Rectangle</a>
    </td>
    <td width="300" valign="top">
      <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/circle.png"><img src="./screenshots/circle.png" alt="circle 2d" width="290"></a>
      <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/2d-shapes/circle.js">Circle</a>
    </td>
  </tr>
</table>

## 3d-shapes

<table>
  <tr>
   <td width="300" valign="top">
      <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/cube.png"><img src="./screenshots/cube.png" alt="cube 3d" width="290"></a>
      <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/cube.js">Cube</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/sphere.png"><img src="./screenshots/sphere.png" alt="sphere 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/sphere.js">Sphere</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/pyramid.png"><img src="./screenshots/pyramid.png" alt="pyramid 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/pyramid.js">Pyramid</a>
   </td>
  </tr>
  <tr>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/points-cube.png"><img src="./screenshots/points-cube.png" alt="points cube 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/points-cube.js">Points cube</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/points-sphere.png"><img src="./screenshots/points-sphere.png" alt="points sphere 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/points-sphere.js">Points sphere</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/points-square.png"><img src="./screenshots/points-square.png" alt="points square 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/points-square.js">Points square</a>
   </td>
  </tr>
  <tr>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/points-instances.png"><img src="./screenshots/points-instances.png" alt="points instances 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/points-instances.js">Points instances</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/square-instances.png"><img src="./screenshots/square-instances.png" alt="square instances 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/square-instances.js">Square instances</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/checkered.png"><img src="./screenshots/checkered.png" alt="checkered 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/checkered.js">Checkered</a>
   </td>
  </tr>
  <tr>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/paraboloid.png"><img src="./screenshots/paraboloid.png" alt="paraboloid of revolution 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/paraboloid.js">Paraboloid of revolution</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/elliptic-paraboloid.png"><img src="./screenshots/elliptic-paraboloid.png" alt="elliptic paraboloid 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/paraboloid.js">Elliptic paraboloid</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/hyperbolic-paraboloid.png"><img src="./screenshots/hyperbolic-paraboloid.png" alt="hyperbolic paraboloid 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/paraboloid.js">Hyperbolic paraboloid</a>
   </td>
  </tr>
  <tr>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/parabolic-cylinder.png"><img src="./screenshots/parabolic-cylinder.png" alt="parabolic cylinder 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/paraboloid.js">Parabolic cylinder</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/horn-torus.png"><img src="./screenshots/horn-torus.png" alt="horn torus 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/horn-torus.js">Horn torus</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/funnel.png"><img src="./screenshots/funnel.png" alt="funnel 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/funnel.js">Funnel</a>
   </td>
  </tr>
  <tr>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/prism.png"><img src="./screenshots/prism.png" alt="prism 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/prism.js">Prism</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/icosahedron.png"><img src="./screenshots/icosahedron.png" alt="icosahedron 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/icosahedron.js">Icosahedron</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/hexagonal-prism.png"><img src="./screenshots/hexagonal-prism.png" alt="hexagonal prism 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/hexagonal-prism.js">Hexagonal prism</a>
   </td>
  </tr>
  <tr>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/square-antiprism.png"><img src="./screenshots/square-antiprism.png" alt="square antiprism 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/square-antiprism.js">Square anti-prism</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/octagonal-antiprism.png"><img src="./screenshots/octagonal-antiprism.png" alt="octagonal antiprism 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/octagonal-antiprism.js">Octagonal anti-prism</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/decagonal-prism.png"><img src="./screenshots/decagonal-prism.png" alt="decagonal prism 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/decagonal-prism.js">Decagonal prism</a>
   </td>
  </tr>
  <tr>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/wave-plane.png"><img src="./screenshots/wave-plane.png" alt="wave plane 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/wave-plane.js">Wave-plane</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/wave-globe.png"><img src="./screenshots/wave-globe.png" alt="wave globe 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/wave-globe.js">Wave-globe</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/noise-plane.png"><img src="./screenshots/noise-plane.png" alt="noise plane 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/noise-plane.js">Noise plane</a>
   </td>
  </tr>
  <tr>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/blocks-instances.png"><img src="./screenshots/blocks-instances.png" alt="blocks instances 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/blocks-instances.js">Blocks instances</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/screens.png"><img src="./screenshots/screens.png" alt="screens 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/screens.js">Screens</a>
   </td>
   <td width="300" valign="top">
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/stars.png"><img src="./screenshots/stars.png" alt="stars 3d" width="290"></a>
    <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/3d-shapes/stars.js">Stars</a>
   </td>
  </tr>
</table>

## animation

<table>
 <tr>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/bloaby-sphere.png"><img src="./screenshots/bloaby-sphere.png" alt="bloaby sphere 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/bloaby-sphere.js">Bloaby sphere</a>
  </td>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/earth.png"><img src="./screenshots/earth.png" alt="earth 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/earth.js">Earth</a>
  </td>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/rocks.png"><img src="./screenshots/rocks.png" alt="rocks 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/rocks.js">Spinning ring of rocks</a>
  </td>
 </tr>
 <tr>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/vaporwave.png"><img src="./screenshots/vaporwave.png" alt="vaporwave 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/vaporwave.js">Vaporwave</a>
  </td>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/vaporwave-2.png"><img src="./screenshots/vaporwave-2.png" alt="vaporwave-2 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/vaporwave-2.js">Vaporwave-2</a>
  </td>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/black-flag.png"><img src="./screenshots/black-flag.png" alt="black-flag 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/black-flag.js">Black flag</a>
  </td>
 </tr>
 <tr>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/tvs.png"><img src="./screenshots/tvs.png" alt="tvs 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/tvs.js">TVs</a>
  </td>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/hole.png"><img src="./screenshots/hole.png" alt="hole 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/hole.js">Hole</a>
  </td>
  <td width="300" valign="top">
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/screenshots/audio.png"><img src="./screenshots/audio.png" alt="audio 3d" width="290"></a>
   <a href="https://gitlab.com/pragalakis/webgl-playground/blob/master/animation/audio.js">Audio</a>
  </td>
 </tr>
</table>
